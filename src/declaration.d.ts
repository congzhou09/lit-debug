import { SimpleGreeting } from './app';

declare module '*.scss'; // scss module

declare global {
  interface HTMLElementTagNameMap {
    'simple-greeting': SimpleGreeting;
  }
}
