module.exports = (curConfig) => {
  curConfig.devServer = { ...curConfig.devServer, port: 2009 };
  return curConfig;
};
