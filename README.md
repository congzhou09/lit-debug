### intro

○ Used for [lit](https://lit.dev/) features verification.

### development

○ Run "yarn install" to install dependencies.

○ Run "yarn vite" or "yarn dev" to start local (vite or webpack) dev-server.

○ Visit "http://localhost:2023" in browser.

※ Refer to [brick-cli-base](https://www.npmjs.com/package/brick-cli-base) for project configs.
